import * as types from "./actionTypes";

export default function app(state = stateINIT, action = {}) {
 
  switch (action.type) {
    case types.FILMES:
      return Object.assign({}, state, {lista_filmes: action.lista_filmes});

    default:
      delete action.type;
      return Object.assign(state, action);
  }
}
const stateINIT = {
  lista_filmes: []
};
