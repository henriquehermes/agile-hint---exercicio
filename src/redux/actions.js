const axios = require('axios');

function setLista(data) {
  return {
    type: "FILMES",
    lista_filmes: data
  };
}

export function getData(dispatch) {
  axios.get('https://facebook.github.io/react-native/movies.json')
    .then(function (response) {
      // handle success
      dispatch(setLista(response.data))
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
}