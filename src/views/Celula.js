import React, { Component } from 'react';
import { Image, StyleSheet, Text, View, TouchableOpacity } from 'react-native';

export default class Celula extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <TouchableOpacity style={styles.container} onPress={() => alert(this.props.filme.releaseYear)}>
        <View style={styles.containerIcone} >
          <Image
            style={{ width: 30, height: 30 }} source={require('../../img/movie.png')}
          />
        </View>
        <View style={styles.containerText}>
          <Text style={styles.text}>{this.props.filme.title}</Text>
          <Text style={styles.text}>{this.props.filme.releaseYear}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignSelf: "stretch",
    height: 70,
    borderLeftColor: "#0a57a7",
    borderLeftWidth: 4,
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  containerIcone: {
    width: 50,
    margin: 10,
    alignSelf: "stretch",
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: {
    marginLeft: 15,
    color: "#0a57a7"
  },
  containerText: {
    alignSelf: "stretch",
    justifyContent: 'center',
    alignItems: 'flex-start',
  }
});
