import React, { Component } from 'react';
import { ActivityIndicator, StyleSheet, FlatList, View, ScrollView } from 'react-native';
import * as actions from "../redux/actions";
import Celula from "./Celula";
import ElevatedView from 'react-native-elevated-view'

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lista_filmes: [],
      load: true
    };
    actions.getData(this.props.screenProps.store.dispatch)
  }

  componentDidMount() {
    this.unsubscribe = this.props.screenProps.store.subscribe(() => {
      var store = this.props.screenProps.store.getState();
      if (store.lista_filmes != this.state.lista_filmes) this.setState({ lista_filmes: store.lista_filmes, load: false });
    });
  }

  componentWillUnmount() {
    if (this.unsubscribe) this.unsubscribe();
  }

  render() {
    return (
      <ScrollView style={styles.ScrollView}>
        <View style={styles.container}>
          {this.state.load && <ActivityIndicator size="large" color="#0a57a7" />}
          <FlatList
            data={this.state.lista_filmes.movies}
            style={styles.FlatList}
            renderItem={({ item }) => <View style={styles.Celula}>
              <ElevatedView elevation={2}>
                <Celula filme={item} />
              </ElevatedView>

            </View>}
          />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  ScrollView: {
    flex: 1,
    alignSelf: "stretch"
  },
  FlatList: {
    flex: 1,
    alignSelf: "stretch"
  },
  Celula: {
    flex: 1,
    alignSelf: "stretch",
    padding: 10
  }
});
