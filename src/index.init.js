import React from "react";
import { View } from "react-native";
import { StackNavigator } from "react-navigation";
import { Provider } from "react-redux";
import { registerScreens } from "./index.pages.js";
import { createStore } from "redux";
import reducers from "./redux/reducer";
import * as actions from "./redux/actions";

var pgs = registerScreens();
var store = createStore(reducers);
var Nav = StackNavigator(pgs);

console.disableYellowBox = true;

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    let screenProps = {
      store: store,
      actions: actions,
      dispatch: store.dispatch
    };    
    return (
      <View
        style={{
          alignSelf: "stretch",
          flex: 1
        }}
      >
        <Provider store={store}>
          <Nav
            {...this.props}
            store={store}
            screenProps={screenProps}
            onNavigationStateChange={null}
            store={store}
          />
        </Provider>
      </View>
    );
  }
}
