import { connect } from "react-redux";
import Home from "./views/Home";
import { TouchableOpacity, Image } from 'react-native';
import React from 'react';

export function registerScreens() {
  return {
    Home: {
      screen: connect()(Home),
      navigationOptions: {
        headerLeft: (
          <TouchableOpacity
            onPress={() => alert('Botão Menu!')}
            style={{ width: 45, height: 40, alignItems: 'center', justifyContent: 'center'}}
            color="#fff"
          >
            <Image
              style={{ width: 25, height: 25 }} source={require('../img/menu.png')}
            />
          </TouchableOpacity>
        ),
        headerTitle: 'Agile Hint',
        headerTitleStyle: {
          color: "#FFF",
          textAlign: "center",
          flex: 1
        },
        headerStyle:
        {
          backgroundColor: '#0a57a7'
        },
        headerRight: (
          <TouchableOpacity
            onPress={() => alert('Botão Pesquisar!')}
            style={{ width: 45, height: 40, alignItems: 'center', justifyContent: 'center'}}
            color="#fff"
          >
            <Image
              style={{ width: 25, height: 25 }} source={require('../img/magnify.png')}
            />
          </TouchableOpacity>
        ),
      }
    }
  };
}